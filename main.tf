provider "aws" {
  region = "eu-west-1"
}

resource "aws_instance" "projet_annuel" {
  ami           = "ami-079d9017cb651564d"
  instance_type = "t2.micro"
  #availability_zone = "eu-west-1b"
  key_name = "deploy_glpi"

  tags = {
    Name = "GLPI_APP"
  }
   network_interface {
    network_interface_id = aws_network_interface.interface_1.id
    device_index         = 0
  }
}

resource "aws_vpc" "terraform_vpc" {
  cidr_block       = "192.168.0.0/16"
  #instance_tenancy = "default"

  tags = {
    Name = "terraform_vpc"
  }
}

resource "aws_subnet" "subnet_1" {
  vpc_id     = aws_vpc.terraform_vpc.id
  cidr_block = "192.168.1.0/24"

  tags = {
    Name = "LaboInfra"
  }
}

resource "aws_network_interface" "interface_1" {
  subnet_id       = aws_subnet.subnet_1.id
  private_ips     = ["192.168.1.25"]
  security_groups = [aws_security_group.allow_http_ssh.id]

}

resource "aws_security_group" "allow_http_ssh" {
  name        = "allow_http_ssh"
  description = "Allow http_ssh inbound traffic"
  vpc_id      = aws_vpc.terraform_vpc.id

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

   ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_http_ssh"
  }
}

resource "aws_route_table" "lab_route_table" {
  vpc_id = aws_vpc.terraform_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gateway.id
  }
  
}

resource "aws_internet_gateway" "gateway" {
  vpc_id = aws_vpc.terraform_vpc.id

  tags = {
    Name = "Esgi_gateway"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id         = aws_subnet.subnet_1.id
  route_table_id = aws_route_table.lab_route_table.id
}

resource "aws_eip" "one" {
  vpc                       = true
  network_interface         = aws_network_interface.interface_1.id
  associate_with_private_ip = "192.168.1.25"
  depends_on                = [aws_internet_gateway.gateway]
}
